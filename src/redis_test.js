var redis = require('redis')

var client = redis.createClient();

client.on('connect', function () {
    console.log('Connecté à la base Redis');
});

client.set('host', 'localhost', function (err, reply) {
    if (err !== null) {
        console.warn('Erreur : ' + err);
    }
});

client.set('ticketFormat', JSON.stringify({
    'categorie': {
        'type': 'list',
        'title': 'Catégorie',
        'items': [
            'espaces verts',
            'voirie',
            'batiment'
        ],
        'mails': [
            'mail@adress.com',
            'mail2@adress.com',
            'mail_autre@adress.com',
        ]
    },
    'status': {
        'type': 'list',
        'title': 'Priorité',
        'items': [
            'critique',
            'urgente',
            'faible',
            'moyenne',
            'résolu'
        ],
        'mails': [
            'mail@adress.com',
            'mail2@adress.com',
            'mail_autre@adress.com'
        ]
    },
    'description': {
        'type': 'text',
        'title': 'Description'
    },
    'image': {
        'type': 'image',
        'title': 'Photo(s)',
        'optional': 'true',
        'multiple': 'true'
    },
}), function (err, reply) {
    if(err !==null) {
        console.warn('Erreur : ' + err);
    }
});

client.exists('ticketFormat', function (err, reply) {
   if(reply === 1) {
       console.log("Clé ticketFormat trouvée");
   } else {
       console.log("La clé ticketFormat n\'existe pas");
   }
});

client.get('host', function (err, reply) {
    if(err === null) {
        console.log('Valeur de host : ' + reply);
    }
});

client.get('ticketFormat', function (err, reply) {
    if(err === null) {
        console.log('Valeur de ticketFormat : ');
        console.log(JSON.parse(reply));
    }
});

client.del('host', function (err, reply) {
    if(err === null) {
        console.log('La clé host a été supprimée');
    }
});

